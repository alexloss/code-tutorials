# Loading Bar

## Goal

Get familiar with common double-underscore "dunder" functions.

## Challenge

Create a stylish loading bar that can easily be printed. To achieve that goal and test it, we have a `TaskList` class. When created, a `task_list` element of that class should be an iterator that goes through a fixed number of tasks. Execute each task and display the progress after each task is done.

