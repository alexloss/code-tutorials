import time
from typing import Collection

class Task:
    """Is supposed to do some stuff"""
    def do_stuff(self):
        time.sleep(.2)

class MyTask(Task):
    """A derived Task class"""
    # TODO be creative if you wish to.
    pass

class TaskList:
    """Is an iterable of tasks to perform.

    The exercise needs us to know how many tasks 
    are queued up. If using a task_iterator, it needs
    to have a set length (like a Collection)
    """
    def __init__(self, 
                 n_tasks:int=0,
                 task_iter:Collection[Task]=[]):
        """
        If `number_of_tasks > 0`, make a list of `Task` elements of that length.
        Otherwise, iterate through the `task_iterator`
        """
        raise NotImplementedError
        pass

    def __len__(self):
        raise NotImplementedError
        pass

    def __iter__(self):
        raise NotImplementedError
        pass

    def __next__(self):
        raise StopIteration()

