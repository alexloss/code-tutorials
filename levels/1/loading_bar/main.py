import task
from task import TaskList, Task
from loading_bar import LoadingBar

LEN_TASKS = 5
task_list = TaskList(LEN_TASKS)

for task in task_list:
    task.do_stuff()
    # TODO display how much progress has been made

