from loading_bar.task import TaskList, Task
from loading_bar.loading_bar import LoadingBar
from random import randint

def test_tasks():
    assert all(isinstance(task, Task) for task in TaskList(3, [Task()]))
    _len = randint(3, 69)
    assert len(TaskList(_len, [Task()])) == _len
    assert len(TaskList(task_iter=[Task()]*_len)) == _len

def test_loading_bar():
    assert(str(LoadingBar(0, showPercent=False))) == "[          ]"
    assert(str(LoadingBar(0, width=0, showPercent=False))) == "[]"
    assert(str(LoadingBar(10, 15, width=3, showPercent=False))) == "[## ]"
    assert(str(LoadingBar(10, 15, showPercent=False))) == "[#######   ]"
    assert(str(LoadingBar(10, 15))) == "[#######   ]  66%"
