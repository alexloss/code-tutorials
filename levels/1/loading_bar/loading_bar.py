
class LoadingBar:
    """Serves to display a nice loading bar.

    >>> print(LoadingBar(5))
    [#         ]   5%
    >>> print(LoadingBar(10, out_of=15))
    [#######   ]  66%
    >>> print(LoadingBar(5, width=2))
    [# ]   5%
    """
    def __init__(self, done:int, out_of:int=100, width:int=10, showPercent=True):
        raise NotImplementedError
        pass

    def __repr__(self):
        # TODO
        pass

    def __str__(self):
        # TODO
        pass
