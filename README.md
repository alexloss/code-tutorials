# Challenges

My training challenges written for different languages, different needs...

It includes very basic exercises, some of them are there to simply understand 
the syntax of a language, not about programming.

<!--
## Summary

Auto-generated list of challenges:
-->

Each challenge has a `main` file (`main.py`, `main.c`, `main.rs` ...) which is 
the endpoint of a challenge, and a README file to explain the challenge and
guide you.

## Requirements

Python >= 3.8

```
pip install venv pytest numpy pandas
```

## Testing

To know if you completed a challenge, there should be a test file to verify how far you have come.

#### Python testing

1. Install `pytest`.
   `pip install pytest`
2. Go to the challenge directory and run `pytest`.

