# Be sure to have numpy and matplotlib installed first:
# `pip install numpy matplotlib`
import numpy as np
import matplotlib as plt


p0 = np.array([0., 0.])
v0 = np.array([0., 0.])

g = 9.81

i = np.array([1., 0.])
j = np.array([0., 1.])